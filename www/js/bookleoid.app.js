// Initialize your app
var bookleoidApp = new Framework7({
    material: true,
    template7Pages: true,
    template7Data: bookleoidData()
});

// Export selectors engine
var $$ = Dom7;

// Add view
var mainView = bookleoidApp.addView('.view-main', {
    // Because we use fixed-through navbar we can enable dynamic navbar
    dynamicNavbar: true
});

mainView.refreshPage();

$$(document).on('click', '.link, .item-link, .button', function () {
    window.plugins.deviceFeedback.isFeedbackEnabled(function (feedback) {
        if (feedback.acoustic)
            window.plugins.deviceFeedback.acoustic();
        else
            window.plugins.deviceFeedback.haptic();
    });
});

bookleoidApp.onPageInit('tour', function (page) {
    $('.cycle').cyclotron();
});

$$(document).on('deviceready', function () {
    navigator.splashscreen.hide();
});

$$('.alert-text').on('click', function () {
    bookleoidApp.alert('Correo enviado exitosamente');
});