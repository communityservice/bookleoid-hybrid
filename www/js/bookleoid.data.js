var bookleoidData = function () {
    return {
        'page:mainMenu': {

            /*title: 'Beatas de Venezuela',
            sections: [
                {
                    name: 'María de San José Alvarado',
                    img: 'img/maria_de_san_jose.jpg',
                    link: 'subMenu.html',
                    linkContext: 'subMenuMariaAlvarado'
                },
                {
                    name: 'Candelaria de San José',
                    img: 'img/candelaria_de_san_jose.jpg',
                    link: 'subMenu.html',
                    linkContext: 'subMenuMariaCandelaria'
                }
            ]*/

            /*CAMINO A LOS ALTARES - MENUS*/
            title: 'Camino a los Altares',
            sections: [
                {
                    name: 'Venerable Madre Emilia de San José',
                    img: 'img/caminoALosAltares/madre_emilia_de_san_jose.png',
                    link: 'subMenu.html',
                    linkContext: 'subMenuEmiliaSanJose'
                },
                {
                    name: 'Venerable Madre Carmen Rendiles',
                    img: 'img/caminoALosAltares/madre_carmen_rendiles.jpg',
                    link: 'subMenu.html',
                    linkContext: 'subMenuCarmenRendiles'
                },
                {
                    name: 'Siervo de Dios, Mons. Salvador Montes de Oca',
                    img: 'img/caminoALosAltares/salvador_montes_de_oca.jpg',
                    link: 'subMenu.html',
                    linkContext: 'subMenuMontesDeOca'
                },
                {
                    name: 'Siervo de Dios, Mons. Sixto Sosa',
                    img: 'img/caminoALosAltares/sixto_sosa.jpg',
                    link: 'subMenu.html',
                    linkContext: 'subMenuSixtoSosa'
                },
                {
                    name: 'Sierva de Dios, Georgina Josefa Febres Cordero',
                    img: 'img/caminoALosAltares/georgina_josefa_febres_cordero.jpg',
                    link: 'subMenu.html',
                    linkContext: 'subMenuGeorginaFebres'
                },
                {
                    name: 'Siervo de Dios, Mons. Arturo Celestino Álvarez',
                    img: 'img/caminoALosAltares/arturo_celestino_alvarez.jpg',
                    link: 'subMenu.html',
                    linkContext: 'subMenuArturoCelestino'
                },
                {
                    name: 'Sierva de Dios, Madre Marcelina de San José Aveledo Ostos',
                    img: 'img/caminoALosAltares/marcelina_de_san_jose_aveledo_ostos.jpg',
                    link: 'subMenu.html',
                    linkContext: 'subMenuMarcelinaSanJose'
                },
                {
                    name: 'Sierva de Dios, Maria Geralda Guerrero García de Piñeiro',
                    img: 'img/caminoALosAltares/maria_geralda_guerrero_garcia_de_pineiro.jpg',
                    link: 'subMenu.html',
                    linkContext: 'subMenuMariaGeraldaGuerrero'
                },
                {
                    name: 'Siervo de Dios, Tomás Antonio Sanmiguel Díaz',
                    img: 'img/caminoALosAltares/tomas_antonio_sanmiguel_diaz.jpg',
                    link: 'subMenu.html',
                    linkContext: 'subMenuTomasSanmiguel'
                },
                {
                    name: 'Siervos de Dios, Arístides Calvani Silva y Adelia Abbo Fontana (Matrimonio)',
                    img: 'img/caminoALosAltares/adelia_abbo_fontana_de_calvani.jpg',
                    link: 'subMenu.html',
                    linkContext: 'subMenuCalvaniFontana'
                },
                {
                    name: 'Siervo de Dios, Luis Rafael Tinoco Yépez',
                    img: 'img/caminoALosAltares/luis_rafael_tinoco_yepez.jpg',
                    link: 'subMenu.html',
                    linkContext: 'subMenuLuisTinoco'
                },
                {
                    name: 'Sierva de Dios, Hna. María Israel Bogotá Baquero',
                    img: 'img/caminoALosAltares/maria_israel_bogota_baquero.jpg',
                    link: 'subMenu.html',
                    linkContext: 'subMenuMariaBogota'
                },
                {
                    name: 'Siervo de Dios, Tomás Morales Pérez',
                    img: 'img/caminoALosAltares/tomas_morales_perez.jpg',
                    link: 'subMenu.html',
                    linkContext: 'subMenuTomasMorales'
                },
                {
                    name: 'Justo Vicente López Aveledo',
                    img: 'img/caminoALosAltares/justo_vicente_lopez_aveledo.jpg',
                    link: 'subMenu.html',
                    linkContext: 'subMenuJustoLopez'
                },
                {
                    name: 'Jesús María Pellín',
                    img: 'img/caminoALosAltares/jesus_maria_pellin.jpg',
                    link: 'subMenu.html',
                    linkContext: 'subMenuJesusPellin'
                },
                {
                    name: 'Marcos Sergio Godoy',
                    img: 'img/caminoALosAltares/marcos_sergio_godoy.jpg',
                    link: 'subMenu.html',
                    linkContext: 'subMenuMarcosGodoy'
                },
                {
                    name: 'Domingo Roa Perez',
                    img: 'img/caminoALosAltares/domingo_roa_perez.jpg',
                    link: 'subMenu.html',
                    linkContext: 'subMenuDomingoPerez'
                },
                {
                    name: 'Mons. Eduardo Boza Masvidal',
                    img: 'img/caminoALosAltares/eduardo_boza_masvidal.jpg',
                    link: 'subMenu.html',
                    linkContext: 'subMenuEduardoBoza'
                }
            ]
        },
        /*CAMINO A LOS ALTARES - MENUS*/

        /*
        'subMenuMariaAlvarado': {
            title: 'Vida y Obra',
            sections: [
                {
                    name: 'Vida',
                    img: 'img/i-form-name-ios.svg'
                },
                {
                    name: 'Obra',
                    img: 'img/i-form-name-ios.svg'
                },
                {
                    name: 'Muerte',
                    img: 'img/i-form-name-ios.svg'
                }
            ]
        },
        'subMenuMariaCandelaria': {
            title: 'Vida y Obra',
            sections: [
                {
                    name: 'Vida',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'vidaMariaCandelaria'
                },
                {
                    name: 'Obra',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'obraMariaCandelaria'
                },
                {
                    name: 'Fortuna',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'fortunaMariaCandelaria'
                },
                {
                    name: 'Muerte',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'muerteMariaCandelaria'
                }
            ],
            tour:[
                {
                    name:'Tour',
                    img: 'img/i-form-name-ios.svg',
                    linkContext: 'tourMariaCanelaria',
                    values:[
                        {
                            name:'value1',
                            linkContext:'tourOneMariaCandelaria'
                        },
                        {
                            name:'value2'
                        }
                    ]
                }
            ]
        },
         */
        /*
        'tourOneMariaCandelaria':{
            tour_name:'value1',
            img:'img/panoramic360.jpg'
        },

        'vidaMariaCandelaria':{
            title:'Vida',
            html:'<p>Vida maria candelaria</p>'
        },

        'obraMariaCandelaria':{
            title:'Obra',
            html:'<p>Obra maria candelaria</p>'
        },
        */

        /*CAMINO A LOS ALTARES - SUBMENUS*/
        'subMenuEmiliaSanJose': {
            title: 'Seleccione una opción',
            sections: [
                {
                    name: 'Historia',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'historiaEmiliaSanJose'
                },
                {
                    name: 'Santuario',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'santuarioEmiliaSanJose'
                },
                {
                    name: 'Oración',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'oracionEmiliaSanJose'
                },
                {
                    name: 'Multimedia',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'multimediaEmiliaSanJose'
                },
                {
                    name: 'Eventos',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'eventosEmiliaSanJose'
                },
                /*{
                    name: ' Ubicación',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'ubicacionEmiliaSanJose'
                },*/
                {
                    name: 'Contacto',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'contactoEmiliaSanJose'
                },
                {
                    name: 'Donativo',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'donativoEmiliaSanJose'
                }
            ],
            tour:[
                {
                    name:'Tour',
                    img: 'img/i-form-name-ios.svg',
                    linkContext: 'tourEmiliaSanJose',
                    values:[
                        {
                            name:'Tour Uno',
                            linkContext:'tourUnoEmiliaSanJose'
                        }
                    ]
                }
            ],
            ubicacion:
            {
                name: ' Ubicación',
                img: 'img/i-form-name-ios.svg',
                linkContext:'ubicacionEmiliaSanJose'
            }
        },
        'subMenuCarmenRendiles': {
            title: 'Seleccione una opción',
            sections: [
                {
                    name: 'Historia',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'historiaCarmenRendiles'
                },
                {
                    name: 'Santuario',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'santuarioCarmenRendiles'
                },
                {
                    name: 'Oración',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'oracionCarmenRendiles'
                },
                {
                    name: 'Multimedia',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'multimediaCarmenRendiles'
                },
                {
                    name: 'Eventos',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'eventosCarmenRendiles'
                },
                {
                    name: ' Ubicación',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'ubicacionCarmenRendiles'
                },
                {
                    name: 'Contacto',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'contactoCarmenRendiles'
                },
                {
                    name: 'Donativo',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'donativoCarmenRendiles'
                }
            ],
            tour:[
                {
                    name:'Tour',
                    img: 'img/i-form-name-ios.svg',
                    linkContext: 'tourCarmenRendiles',
                    values:[
                        {
                            name:'Tour Uno',
                            linkContext:'tourUnoCarmenRendiles'
                        }
                    ]
                }
            ]
        },
        'subMenuMontesDeOca': {
            title: 'Seleccione una opción',
            sections: [
                {
                    name: 'Historia',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'historiaMontesDeOca'
                },
                {
                    name: 'Santuario',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'santuarioMontesDeOca'
                },
                {
                    name: 'Oración',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'oracionMontesDeOca'
                },
                {
                    name: 'Multimedia',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'multimediaMontesDeOca'
                },
                {
                    name: 'Eventos',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'eventosMontesDeOca'
                },
                {
                    name: ' Ubicación',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'ubicacionMontesDeOca'
                },
                {
                    name: 'Contacto',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'contactoMontesDeOca'
                },
                {
                    name: 'Donativo',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'donativoMontesDeOca'
                }
            ],
            tour:[
                {
                    name:'Tour',
                    img: 'img/i-form-name-ios.svg',
                    linkContext: 'tourMontesDeOca',
                    values:[

                    ]
                }
            ]
        },
        'subMenuSixtoSosa': {
            title: 'Seleccione una opción',
            sections: [
                {
                    name: 'Historia',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'historiaSixtoSosa'
                },
                {
                    name: 'Santuario',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'santuarioSixtoSosa'
                },
                {
                    name: 'Oración',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'oracionSixtoSosa'
                },
                {
                    name: 'Multimedia',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'multimediaSixtoSosa'
                },
                {
                    name: 'Eventos',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'eventosSixtoSosa'
                },
                {
                    name: ' Ubicación',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'ubicacionSixtoSosa'
                },
                {
                    name: 'Contacto',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'contactoSixtoSosa'
                },
                {
                    name: 'Donativo',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'donativoSixtoSosa'
                }
            ],
            tour:[
                {
                    name:'Tour',
                    img: 'img/i-form-name-ios.svg',
                    linkContext: 'tourSixtoSosa',
                    values:[

                    ]
                }
            ]
        },
        'subMenuGeorginaFebres': {
            title: 'Seleccione una opción',
            sections: [
                {
                    name: 'Historia',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'historiaGeoginaFebres'
                },
                {
                    name: 'Santuario',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'santuarioGeoginaFebres'
                },
                {
                    name: 'Oración',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'oracionGeoginaFebres'
                },
                {
                    name: 'Multimedia',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'multimediaGeoginaFebres'
                },
                {
                    name: 'Eventos',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'eventosGeoginaFebres'
                },
                {
                    name: ' Ubicación',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'ubicacionGeoginaFebres'
                },
                {
                    name: 'Contacto',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'contactoGeoginaFebres'
                },
                {
                    name: 'Donativo',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'donativoGeoginaFebres'
                }
            ],
            tour:[
                {
                    name:'Tour',
                    img: 'img/i-form-name-ios.svg',
                    linkContext: 'tourGeoginaFebres',
                    values:[

                    ]
                }
            ]
        },
        'subMenuArturoCelestino': {
            title: 'Seleccione una opción',
            sections: [
                {
                    name: 'Historia',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'historiaArturoCelestino'
                },
                {
                    name: 'Santuario',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'santuarioArturoCelestino'
                },
                {
                    name: 'Oración',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'oracionArturoCelestino'
                },
                {
                    name: 'Multimedia',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'multimediaArturoCelestino'
                },
                {
                    name: 'Eventos',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'eventosArturoCelestino'
                },
                {
                    name: ' Ubicación',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'ubicacionArturoCelestino'
                },
                {
                    name: 'Contacto',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'contactoArturoCelestino'
                },
                {
                    name: 'Donativo',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'donativoArturoCelestino'
                }
            ],
            tour:[
                {
                    name:'Tour',
                    img: 'img/i-form-name-ios.svg',
                    linkContext: 'tourArturoCelestino',
                    values:[

                    ]
                }
            ]
        },
        'subMenuMarcelinaSanJose': {
            title: 'Seleccione una opción',
            sections: [
                {
                    name: 'Historia',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'historiaMarcelinaSanJose'
                },
                {
                    name: 'Santuario',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'santuarioMarcelinaSanJose'
                },
                {
                    name: 'Oración',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'oracionMarcelinaSanJose'
                },
                {
                    name: 'Multimedia',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'multimediaMarcelinaSanJose'
                },
                {
                    name: 'Eventos',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'eventosMarcelinaSanJose'
                },
                {
                    name: ' Ubicación',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'ubicacionMarcelinaSanJose'
                },
                {
                    name: 'Contacto',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'contactoMarcelinaSanJose'
                },
                {
                    name: 'Donativo',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'donativoMarcelinaSanJose'
                }
            ],
            tour:[
                {
                    name:'Tour',
                    img: 'img/i-form-name-ios.svg',
                    linkContext: 'tourMarcelinaSanJose',
                    values:[

                    ]
                }
            ]
        },
        'subMenuMariaGeraldaGuerrero': {
            title: 'Seleccione una opción',
            sections: [
                {
                    name: 'Historia',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'historiaMariaGeraldaGuerrero'
                },
                {
                    name: 'Santuario',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'santuarioMariaGeraldaGuerrero'
                },
                {
                    name: 'Oración',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'oracionMariaGeraldaGuerrero'
                },
                {
                    name: 'Multimedia',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'multimediaMariaGeraldaGuerrero'
                },
                {
                    name: 'Eventos',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'eventosMariaGeraldaGuerrero'
                },
                {
                    name: ' Ubicación',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'ubicacionMariaGeraldaGuerrero'
                },
                {
                    name: 'Contacto',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'contactoMariaGeraldaGuerrero'
                },
                {
                    name: 'Donativo',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'donativoMariaGeraldaGuerrero'
                }
            ],
            tour:[
                {
                    name:'Tour',
                    img: 'img/i-form-name-ios.svg',
                    linkContext: 'tourMariaGeraldaGuerrero',
                    values:[

                    ]
                }
            ]
        },
        'subMenuTomasSanmiguel': {
            title: 'Seleccione una opción',
            sections: [
                {
                    name: 'Historia',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'historiaTomasSanmiguel'
                },
                {
                    name: 'Santuario',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'santuarioTomasSanmiguel'
                },
                {
                    name: 'Oración',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'oracionTomasSanmiguel'
                },
                {
                    name: 'Multimedia',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'multimediaTomasSanmiguel'
                },
                {
                    name: 'Eventos',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'eventosTomasSanmiguel'
                },
                {
                    name: ' Ubicación',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'ubicacionTomasSanmiguel'
                },
                {
                    name: 'Contacto',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'contactoTomasSanmiguel'
                },
                {
                    name: 'Donativo',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'donativoTomasSanmiguel'
                }
            ],
            tour:[
                {
                    name:'Tour',
                    img: 'img/i-form-name-ios.svg',
                    linkContext: 'tourTomasSanmiguel',
                    values:[

                    ]
                }
            ]
        },
        'subMenuCalvaniFontana': {
            title: 'Seleccione una opción',
            sections: [
                {
                    name: 'Historia',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'historiaCalvaniFontana'
                },
                {
                    name: 'Santuario',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'santuarioCalvaniFontana'
                },
                {
                    name: 'Oración',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'oracionCalvaniFontana'
                },
                {
                    name: 'Multimedia',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'multimediaCalvaniFontana'
                },
                {
                    name: 'Eventos',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'eventosCalvaniFontana'
                },
                {
                    name: ' Ubicación',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'ubicacionCalvaniFontana'
                },
                {
                    name: 'Contacto',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'contactoCalvaniFontana'
                },
                {
                    name: 'Donativo',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'donativoCalvaniFontana'
                }
            ],
            tour:[
                {
                    name:'Tour',
                    img: 'img/i-form-name-ios.svg',
                    linkContext: 'tourCalvaniFontana',
                    values:[

                    ]
                }
            ]
        },
        'subMenuLuisTinoco': {
            title: 'Seleccione una opción',
            sections: [
                {
                    name: 'Historia',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'historiaLuisTinoco'
                },
                {
                    name: 'Santuario',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'santuarioLuisTinoco'
                },
                {
                    name: 'Oración',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'oracionLuisTinoco'
                },
                {
                    name: 'Multimedia',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'multimediaLuisTinoco'
                },
                {
                    name: 'Eventos',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'eventosLuisTinoco'
                },
                {
                    name: ' Ubicación',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'ubicacionLuisTinoco'
                },
                {
                    name: 'Contacto',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'contactoLuisTinoco'
                },
                {
                    name: 'Donativo',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'donativoLuisTinoco'
                }
            ],
            tour:[
                {
                    name:'Tour',
                    img: 'img/i-form-name-ios.svg',
                    linkContext: 'tourLuisTinoco',
                    values:[

                    ]
                }
            ]
        },
        'subMenuMariaBogota': {
            title: 'Seleccione una opción',
            sections: [
                {
                    name: 'Historia',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'historiaMariaBogota'
                },
                {
                    name: 'Santuario',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'santuarioMariaBogota'
                },
                {
                    name: 'Oración',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'oracionMariaBogota'
                },
                {
                    name: 'Multimedia',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'multimediaMariaBogota'
                },
                {
                    name: 'Eventos',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'eventosMariaBogota'
                },
                {
                    name: ' Ubicación',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'ubicacionMariaBogota'
                },
                {
                    name: 'Contacto',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'contactoMariaBogota'
                },
                {
                    name: 'Donativo',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'donativoMariaBogota'
                }
            ],
            tour:[
                {
                    name:'Tour',
                    img: 'img/i-form-name-ios.svg',
                    linkContext: 'tourMariaBogota',
                    values:[

                    ]
                }
            ]
        },
        'subMenuTomasMorales': {
            title: 'Seleccione una opción',
            sections: [
                {
                    name: 'Historia',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'historiaTomasMorales'
                },
                {
                    name: 'Santuario',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'santuarioTomasMorales'
                },
                {
                    name: 'Oración',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'oracionTomasMorales'
                },
                {
                    name: 'Multimedia',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'multimediaTomasMorales'
                },
                {
                    name: 'Eventos',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'eventosTomasMorales'
                },
                {
                    name: ' Ubicación',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'ubicacionTomasMorales'
                },
                {
                    name: 'Contacto',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'contactoTomasMorales'
                },
                {
                    name: 'Donativo',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'donativoTomasMorales'
                }
            ],
            tour:[
                {
                    name:'Tour',
                    img: 'img/i-form-name-ios.svg',
                    linkContext: 'tourTomasMorales',
                    values:[

                    ]
                }
            ]
        },
        'subMenuJustoLopez': {
            title: 'Seleccione una opción',
            sections: [
                {
                    name: 'Historia',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'historiaJustoLopez'
                },
                {
                    name: 'Santuario',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'santuarioJustoLopez'
                },
                {
                    name: 'Oración',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'oracionJustoLopez'
                },
                {
                    name: 'Multimedia',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'multimediaJustoLopez'
                },
                {
                    name: 'Eventos',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'eventosJustoLopez'
                },
                {
                    name: ' Ubicación',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'ubicacionJustoLopez'
                },
                {
                    name: 'Contacto',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'contactoJustoLopez'
                },
                {
                    name: 'Donativo',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'donativoJustoLopez'
                }
            ],
            tour:[
                {
                    name:'Tour',
                    img: 'img/i-form-name-ios.svg',
                    linkContext: 'tourJustoLopez',
                    values:[

                    ]
                }
            ]
        },
        'subMenuJesusPellin': {
            title: 'Seleccione una opción',
            sections: [
                {
                    name: 'Historia',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'historiaJesusPellin'
                },
                {
                    name: 'Santuario',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'santuarioJesusPellin'
                },
                {
                    name: 'Oración',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'oracionJesusPellin'
                },
                {
                    name: 'Multimedia',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'multimediaJesusPellin'
                },
                {
                    name: 'Eventos',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'eventosJesusPellin'
                },
                {
                    name: ' Ubicación',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'ubicacionJesusPellin'
                },
                {
                    name: 'Contacto',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'contactoJesusPellin'
                },
                {
                    name: 'Donativo',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'donativoJesusPellin'
                }
            ],
            tour:[
                {
                    name:'Tour',
                    img: 'img/i-form-name-ios.svg',
                    linkContext: 'tourJesusPellin',
                    values:[

                    ]
                }
            ]
        },
        'subMenuMarcosGodoy': {
            title: 'Seleccione una opción',
            sections: [
                {
                    name: 'Historia',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'historiaMarcosGodoy'
                },
                {
                    name: 'Santuario',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'santuarioMarcosGodoy'
                },
                {
                    name: 'Oración',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'oracionMarcosGodoy'
                },
                {
                    name: 'Multimedia',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'multimediaMarcosGodoy'
                },
                {
                    name: 'Eventos',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'eventosMarcosGodoy'
                },
                {
                    name: ' Ubicación',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'ubicacionMarcosGodoy'
                },
                {
                    name: 'Contacto',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'contactoMarcosGodoy'
                },
                {
                    name: 'Donativo',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'donativoMarcosGodoy'
                }
            ],
            tour:[
                {
                    name:'Tour',
                    img: 'img/i-form-name-ios.svg',
                    linkContext: 'tourMarcosGodoy',
                    values:[

                    ]
                }
            ]
        },
        'subMenuDomingoPerez': {
            title: 'Seleccione una opción',
            sections: [
                {
                    name: 'Historia',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'historiaDomingoPerez'
                },
                {
                    name: 'Santuario',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'santuarioDomingoPerez'
                },
                {
                    name: 'Oración',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'oracionDomingoPerez'
                },
                {
                    name: 'Multimedia',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'multimediaDomingoPerez'
                },
                {
                    name: 'Eventos',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'eventosDomingoPerez'
                },
                {
                    name: ' Ubicación',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'ubicacionDomingoPerez'
                },
                {
                    name: 'Contacto',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'contactoDomingoPerez'
                },
                {
                    name: 'Donativo',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'donativoDomingoPerez'
                }
            ],
            tour:[
                {
                    name:'Tour',
                    img: 'img/i-form-name-ios.svg',
                    linkContext: 'tourDomingoPerez',
                    values:[

                    ]
                }
            ]
        },
        'subMenuEduardoBoza': {
            title: 'Seleccione una opción',
            sections: [
                {
                    name: 'Historia',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'historiaEduardoBoza'
                },
                {
                    name: 'Santuario',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'santuarioEduardoBoza'
                },
                {
                    name: 'Oración',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'oracionEduardoBoza'
                },
                {
                    name: 'Multimedia',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'multimediaEduardoBoza'
                },
                {
                    name: 'Eventos',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'eventosEduardoBoza'
                },
                {
                    name: ' Ubicación',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'ubicacionEduardoBoza'
                },
                {
                    name: 'Contacto',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'contactoEduardoBoza'
                },
                {
                    name: 'Donativo',
                    img: 'img/i-form-name-ios.svg',
                    linkContext:'donativoEduardoBoza'
                }
            ],
            tour:[
                {
                    name:'Tour',
                    img: 'img/i-form-name-ios.svg',
                    linkContext: 'tourEduardoBoza',
                    values:[

                    ]
                }
            ]
        },
        /*CAMINO A LOS ALTARES - SUBMENUS*/


        /*CAMINO A LOS ALTARES - OPCIONES*/
        /*EMILIA SAN JOSE - OPCIONES*/
        'historiaEmiliaSanJose':{
            title:'Historia',
            html:'<p>Historia de EmilaSanJose</p>'
        },
        'santuarioEmiliaSanJose':{
            title:'Santuario',
            html:'<p>Santuario de EmilaSanJose</p>'
        },
        'oracionEmiliaSanJose':{
            title:'Oración',
            html:'<p>Oración de EmilaSanJose</p>'
        },
        'multimediaEmiliaSanJose':{
            title:'Multimedia',
            html:'<p>Multimedia de EmilaSanJose</p>'
        },
        'eventosEmiliaSanJose':{
            title:'Eventos',
            html:'<p>Eventos de EmilaSanJose</p>'
        },
        'ubicacionEmiliaSanJose':{
            title:'Ubicación',
            html:'<p>Ubicación de EmilaSanJose</p>'
        },
        'contactoEmiliaSanJose':{
            title:'Contacto',
            html:'<p>Contacto de EmilaSanJose</p>'
        },
        'donativoEmiliaSanJose':{
            title:'Donativo',
            html:'<p>Donativo de EmilaSanJose</p>'
        },
        'tourUnoEmiliaSanJose':{
            tour_name:'Tour Uno',
            img:'img/panoramic360.jpg'
        },

        /*CARMEN RENDILES - OPCIONES*/
        'historiaCarmenRendiles':{
            title:'Historia',
            html:'<p>Historia de CarmenRendiles</p>'
        },
        'santuarioCarmenRendiles':{
            title:'Santuario',
            html:'<p>Santuario de CarmenRendiles</p>'
        },
        'oracionCarmenRendiles':{
            title:'Oración',
            html:'<p>Oración de CarmenRendiles</p>'
        },
        'multimediaCarmenRendiles':{
            title:'Multimedia',
            html:'<p>Multimedia de CarmenRendiles</p>'
        },
        'eventosCarmenRendiles':{
            title:'Eventos',
            html:'<p>Eventos de CarmenRendiles</p>'
        },
        'ubicacionCarmenRendiles':{
            title:'Ubicación',
            html:'<p>Ubicación de CarmenRendiles</p>'
        },
        'contactoCarmenRendiles':{
            title:'Contacto',
            html:'<p>Contacto de CarmenRendiles</p>'
        },
        'donativoCarmenRendiles':{
            title:'Donativo',
            html:'<p>Donativo de CarmenRendiles</p>'
        },
        'tourUnoCarmenRendiles':{
            tour_name:'Tour Uno',
            img:'img/panoramic360.jpg'
        },

        /*MONTES DE OCA - OPCIONES*/
        'historiaMontesDeOca':{
            title:'Historia',
            html:'<p>Historia de MontesDeOca</p>'
        },
        'santuarioMontesDeOca':{
            title:'Santuario',
            html:'<p>Santuario de MontesDeOca</p>'
        },
        'oracionMontesDeOca':{
            title:'Oración',
            html:'<p>Oración de MontesDeOca</p>'
        },
        'multimediaMontesDeOca':{
            title:'Multimedia',
            html:'<p>Multimedia de MontesDeOca</p>'
        },
        'eventosMontesDeOca':{
            title:'Eventos',
            html:'<p>Eventos de MontesDeOca</p>'
        },
        'ubicacionMontesDeOca':{
            title:'Ubicación',
            html:'<p>Ubicación de MontesDeOca</p>'
        },
        'contactoMontesDeOca':{
            title:'Contacto',
            html:'<p>Contacto de MontesDeOca</p>'
        },
        'donativoMontesDeOca':{
            title:'Donativo',
            html:'<p>Donativo de MontesDeOca</p>'
        },

        /*SIXTO SOSA - OPCIONES*/
        'historiaSixtoSosa':{
            title:'Historia',
            html:'<p>Historia de SixtoSosa</p>'
        },
        'santuarioSixtoSosa':{
            title:'Santuario',
            html:'<p>Santuario de SixtoSosa</p>'
        },
        'oracionSixtoSosa':{
            title:'Oración',
            html:'<p>Oración de SixtoSosa</p>'
        },
        'multimediaSixtoSosa':{
            title:'Multimedia',
            html:'<p>Multimedia de SixtoSosa</p>'
        },
        'eventosSixtoSosa':{
            title:'Eventos',
            html:'<p>Eventos de SixtoSosa</p>'
        },
        'ubicacionSixtoSosa':{
            title:'Ubicación',
            html:'<p>Ubicación de SixtoSosa</p>'
        },
        'contactoSixtoSosa':{
            title:'Contacto',
            html:'<p>Contacto de SixtoSosa</p>'
        },
        'donativoSixtoSosa':{
            title:'Donativo',
            html:'<p>Donativo de SixtoSosa</p>'
        },

        /*GEOGINA FEBRES - OPCIONES*/
        'historiaGeoginaFebres':{
            title:'Historia',
            html:'<p>Historia de GeoginaFebres</p>'
        },
        'santuarioGeoginaFebres':{
            title:'Santuario',
            html:'<p>Santuario de GeoginaFebres</p>'
        },
        'oracionGeoginaFebres':{
            title:'Oración',
            html:'<p>Oración de GeoginaFebres</p>'
        },
        'multimediaGeoginaFebres':{
            title:'Multimedia',
            html:'<p>Multimedia de GeoginaFebres</p>'
        },
        'eventosGeoginaFebres':{
            title:'Eventos',
            html:'<p>Eventos de GeoginaFebres</p>'
        },
        'ubicacionGeoginaFebres':{
            title:'Ubicación',
            html:'<p>Ubicación de GeoginaFebres</p>'
        },
        'contactoGeoginaFebres':{
            title:'Contacto',
            html:'<p>Contacto de GeoginaFebres</p>'
        },
        'donativoGeoginaFebres':{
            title:'Donativo',
            html:'<p>Donativo de GeoginaFebres</p>'
        },

        /*ARTURO CELESTINO - OPCIONES*/
        'historiaArturoCelestino':{
            title:'Historia',
            html:'<p>Historia de ArturoCelestino</p>'
        },
        'santuarioArturoCelestino':{
            title:'Santuario',
            html:'<p>Santuario de ArturoCelestino</p>'
        },
        'oracionArturoCelestino':{
            title:'Oración',
            html:'<p>Oración de ArturoCelestino</p>'
        },
        'multimediaArturoCelestino':{
            title:'Multimedia',
            html:'<p>Multimedia de ArturoCelestino</p>'
        },
        'eventosArturoCelestino':{
            title:'Eventos',
            html:'<p>Eventos de ArturoCelestino</p>'
        },
        'ubicacionArturoCelestino':{
            title:'Ubicación',
            html:'<p>Ubicación de ArturoCelestino</p>'
        },
        'contactoArturoCelestino':{
            title:'Contacto',
            html:'<p>Contacto de ArturoCelestino</p>'
        },
        'donativoArturoCelestino':{
            title:'Donativo',
            html:'<p>Donativo de ArturoCelestino</p>'
        },

        /*MARCELINA SAN JOSE - OPCIONES*/
        'historiaMarcelinaSanJose':{
            title:'Historia',
            html:'<p>Historia de MarcelinaSanJose</p>'
        },
        'santuarioMarcelinaSanJose':{
            title:'Santuario',
            html:'<p>Santuario de MarcelinaSanJose</p>'
        },
        'oracionMarcelinaSanJose':{
            title:'Oración',
            html:'<p>Oración de MarcelinaSanJose</p>'
        },
        'multimediaMarcelinaSanJose':{
            title:'Multimedia',
            html:'<p>Multimedia de MarcelinaSanJose</p>'
        },
        'eventosMarcelinaSanJose':{
            title:'Eventos',
            html:'<p>Eventos de MarcelinaSanJose</p>'
        },
        'ubicacionMarcelinaSanJose':{
            title:'Ubicación',
            html:'<p>Ubicación de MarcelinaSanJose</p>'
        },
        'contactoMarcelinaSanJose':{
            title:'Contacto',
            html:'<p>Contacto de MarcelinaSanJose</p>'
        },
        'donativoMarcelinaSanJose':{
            title:'Donativo',
            html:'<p>Donativo de MarcelinaSanJose</p>'
        },

        /*MARIA GERALDA GUERRERO - OPCIONES*/
        'historiaMariaGeraldaGuerrero':{
            title:'Historia',
            html:'<p>Historia de MariaGeraldaGuerrero</p>'
        },
        'santuarioMariaGeraldaGuerrero':{
            title:'Santuario',
            html:'<p>Santuario de MariaGeraldaGuerrero</p>'
        },
        'oracionMariaGeraldaGuerrero':{
            title:'Oración',
            html:'<p>Oración de MariaGeraldaGuerrero</p>'
        },
        'multimediaMariaGeraldaGuerrero':{
            title:'Multimedia',
            html:'<p>Multimedia de MariaGeraldaGuerrero</p>'
        },
        'eventosMariaGeraldaGuerrero':{
            title:'Eventos',
            html:'<p>Eventos de MariaGeraldaGuerrero</p>'
        },
        'ubicacionMariaGeraldaGuerrero':{
            title:'Ubicación',
            html:'<p>Ubicación de MariaGeraldaGuerrero</p>'
        },
        'contactoMariaGeraldaGuerrero':{
            title:'Contacto',
            html:'<p>Contacto de MariaGeraldaGuerrero</p>'
        },
        'donativoMariaGeraldaGuerrero':{
            title:'Donativo',
            html:'<p>Donativo de MariaGeraldaGuerrero</p>'
        },

        /*TOMAS SAN MIGUEL - OPCIONES*/
        'historiaTomasSanmiguel':{
            title:'Historia',
            html:'<p>Historia de TomasSanmiguel</p>'
        },
        'santuarioTomasSanmiguel':{
            title:'Santuario',
            html:'<p>Santuario de TomasSanmiguel</p>'
        },
        'oracionTomasSanmiguel':{
            title:'Oración',
            html:'<p>Oración de TomasSanmiguel</p>'
        },
        'multimediaTomasSanmiguel':{
            title:'Multimedia',
            html:'<p>Multimedia de TomasSanmiguel</p>'
        },
        'eventosTomasSanmiguel':{
            title:'Eventos',
            html:'<p>Eventos de TomasSanmiguel</p>'
        },
        'ubicacionTomasSanmiguel':{
            title:'Ubicación',
            html:'<p>Ubicación de TomasSanmiguel</p>'
        },
        'contactoTomasSanmiguel':{
            title:'Contacto',
            html:'<p>Contacto de TomasSanmiguel</p>'
        },
        'donativoTomasSanmiguel':{
            title:'Donativo',
            html:'<p>Donativo de TomasSanmiguel</p>'
        },

        /*ARISTIDES CALVANI Y ADELIA FONTANA - OPCIONES*/
        'historiaCalvaniFontana':{
            title:'Historia',
            html:'<p>Historia de AristidesCalvani y AdeliaFontana</p>'
        },
        'santuarioCalvaniFontana':{
            title:'Santuario',
            html:'<p>Santuario de AristidesCalvani y AdeliaFontana</p>'
        },
        'oracionCalvaniFontana':{
            title:'Oración',
            html:'<p>Oración de AristidesCalvani y AdeliaFontana</p>'
        },
        'multimediaCalvaniFontana':{
            title:'Multimedia',
            html:'<p>Multimedia de AristidesCalvani y AdeliaFontana</p>'
        },
        'eventosCalvaniFontana':{
            title:'Eventos',
            html:'<p>Eventos de AristidesCalvani y AdeliaFontana</p>'
        },
        'ubicacionCalvaniFontana':{
            title:'Ubicación',
            html:'<p>Ubicación de AristidesCalvani y AdeliaFontana</p>'
        },
        'contactoCalvaniFontana':{
            title:'Contacto',
            html:'<p>Contacto de AristidesCalvani y AdeliaFontana</p>'
        },
        'donativoCalvaniFontana':{
            title:'Donativo',
            html:'<p>Donativo de AristidesCalvani y AdeliaFontana</p>'
        },

        /*LUIS TINOCO - OPCIONES*/
        'historiaLuisTinoco':{
            title:'Historia',
            html:'<p>Historia de LuisTinoco</p>'
        },
        'santuarioLuisTinoco':{
            title:'Santuario',
            html:'<p>Santuario de LuisTinoco</p>'
        },
        'oracionLuisTinoco':{
            title:'Oración',
            html:'<p>Oración de LuisTinoco</p>'
        },
        'multimediaLuisTinoco':{
            title:'Multimedia',
            html:'<p>Multimedia de LuisTinoco</p>'
        },
        'eventosLuisTinoco':{
            title:'Eventos',
            html:'<p>Eventos de LuisTinoco</p>'
        },
        'ubicacionLuisTinoco':{
            title:'Ubicación',
            html:'<p>Ubicación de LuisTinoco</p>'
        },
        'contactoLuisTinoco':{
            title:'Contacto',
            html:'<p>Contacto de LuisTinoco</p>'
        },
        'donativoLuisTinoco':{
            title:'Donativo',
            html:'<p>Donativo de LuisTinoco</p>'
        },

        /*MARIA BOGOTA - OPCIONES*/
        'historiaMariaBogota':{
            title:'Historia',
            html:'<p>Historia de MariaBogota</p>'
        },
        'santuarioMariaBogota':{
            title:'Santuario',
            html:'<p>Santuario de MariaBogota</p>'
        },
        'oracionMariaBogota':{
            title:'Oración',
            html:'<p>Oración de MariaBogota</p>'
        },
        'multimediaMariaBogota':{
            title:'Multimedia',
            html:'<p>Multimedia de MariaBogota</p>'
        },
        'eventosMariaBogota':{
            title:'Eventos',
            html:'<p>Eventos de MariaBogota</p>'
        },
        'ubicacionMariaBogota':{
            title:'Ubicación',
            html:'<p>Ubicación de MariaBogota</p>'
        },
        'contactoMariaBogota':{
            title:'Contacto',
            html:'<p>Contacto de MariaBogota</p>'
        },
        'donativoMariaBogota':{
            title:'Donativo',
            html:'<p>Donativo de MariaBogota</p>'
        },

        /*TOMAS MORALES - OPCIONES*/
        'historiaTomasMorales':{
            title:'Historia',
            html:'<p>Historia de TomasMorales</p>'
        },
        'santuarioTomasMorales':{
            title:'Santuario',
            html:'<p>Santuario de TomasMorales</p>'
        },
        'oracionTomasMorales':{
            title:'Oración',
            html:'<p>Oración de TomasMorales</p>'
        },
        'multimediaTomasMorales':{
            title:'Multimedia',
            html:'<p>Multimedia de TomasMorales</p>'
        },
        'eventosTomasMorales':{
            title:'Eventos',
            html:'<p>Eventos de TomasMorales</p>'
        },
        'ubicacionTomasMorales':{
            title:'Ubicación',
            html:'<p>Ubicación de TomasMorales</p>'
        },
        'contactoTomasMorales':{
            title:'Contacto',
            html:'<p>Contacto de TomasMorales</p>'
        },
        'donativoTomasMorales':{
            title:'Donativo',
            html:'<p>Donativo de TomasMorales</p>'
        },

        /*JUSTO LOPEZ - OPCIONES*/
        'historiaJustoLopez':{
            title:'Historia',
            html:'<p>Historia de JustoLopez</p>'
        },
        'santuarioJustoLopez':{
            title:'Santuario',
            html:'<p>Santuario de JustoLopez</p>'
        },
        'oracionJustoLopez':{
            title:'Oración',
            html:'<p>Oración de JustoLopez</p>'
        },
        'multimediaJustoLopez':{
            title:'Multimedia',
            html:'<p>Multimedia de JustoLopez</p>'
        },
        'eventosJustoLopez':{
            title:'Eventos',
            html:'<p>Eventos de JustoLopez</p>'
        },
        'ubicacionJustoLopez':{
            title:'Ubicación',
            html:'<p>Ubicación de JustoLopez</p>'
        },
        'contactoJustoLopez':{
            title:'Contacto',
            html:'<p>Contacto de JustoLopez</p>'
        },
        'donativoJustoLopez':{
            title:'Donativo',
            html:'<p>Donativo de JustoLopez</p>'
        },

        /*JESUS PELLIN - OPCIONES*/
        'historiaJesusPellin':{
            title:'Historia',
            html:'<p>Historia de JesusPellin</p>'
        },
        'santuarioJesusPellin':{
            title:'Santuario',
            html:'<p>Santuario de JesusPellin</p>'
        },
        'oracionJesusPellin':{
            title:'Oración',
            html:'<p>Oración de JesusPellin</p>'
        },
        'multimediaJesusPellin':{
            title:'Multimedia',
            html:'<p>Multimedia de JesusPellin</p>'
        },
        'eventosJesusPellin':{
            title:'Eventos',
            html:'<p>Eventos de JesusPellin</p>'
        },
        'ubicacionJesusPellin':{
            title:'Ubicación',
            html:'<p>Ubicación de JesusPellin</p>'
        },
        'contactoJesusPellin':{
            title:'Contacto',
            html:'<p>Contacto de JesusPellin</p>'
        },
        'donativoJesusPellin':{
            title:'Donativo',
            html:'<p>Donativo de JesusPellin</p>'
        },

        /*MARCOS GODOY - OPCIONES*/
        'historiaMarcosGodoy':{
            title:'Historia',
            html:'<p>Historia de MarcosGodoy</p>'
        },
        'santuarioMarcosGodoy':{
            title:'Santuario',
            html:'<p>Santuario de MarcosGodoy</p>'
        },
        'oracionMarcosGodoy':{
            title:'Oración',
            html:'<p>Oración de MarcosGodoy</p>'
        },
        'multimediaMarcosGodoy':{
            title:'Multimedia',
            html:'<p>Multimedia de MarcosGodoy</p>'
        },
        'eventosMarcosGodoy':{
            title:'Eventos',
            html:'<p>Eventos de MarcosGodoy</p>'
        },
        'ubicacionMarcosGodoy':{
            title:'Ubicación',
            html:'<p>Ubicación de MarcosGodoy</p>'
        },
        'contactoMarcosGodoy':{
            title:'Contacto',
            html:'<p>Contacto de MarcosGodoy</p>'
        },
        'donativoMarcosGodoy':{
            title:'Donativo',
            html:'<p>Donativo de MarcosGodoy</p>'
        },

        /*DOMINGO PEREZ - OPCIONES*/
        'historiaDomingoPerez':{
            title:'Historia',
            html:'<p>Historia de DomingoPerez</p>'
        },
        'santuarioDomingoPerez':{
            title:'Santuario',
            html:'<p>Santuario de DomingoPerez</p>'
        },
        'oracionDomingoPerez':{
            title:'Oración',
            html:'<p>Oración de DomingoPerez</p>'
        },
        'multimediaDomingoPerez':{
            title:'Multimedia',
            html:'<p>Multimedia de DomingoPerez</p>'
        },
        'eventosDomingoPerez':{
            title:'Eventos',
            html:'<p>Eventos de DomingoPerez</p>'
        },
        'ubicacionDomingoPerez':{
            title:'Ubicación',
            html:'<p>Ubicación de DomingoPerez</p>'
        },
        'contactoDomingoPerez':{
            title:'Contacto',
            html:'<p>Contacto de DomingoPerez</p>'
        },
        'donativoDomingoPerez':{
            title:'Donativo',
            html:'<p>Donativo de DomingoPerez</p>'
        },

        /*EDUARDO BOZA - OPCIONES*/
        'historiaEduardoBoza':{
            title:'Historia',
            html:'<p>Historia de EduardoBoza</p>'
        },
        'santuarioEduardoBoza':{
            title:'Santuario',
            html:'<p>Santuario de EduardoBoza</p>'
        },
        'oracionEduardoBoza':{
            title:'Oración',
            html:'<p>Oración de EduardoBoza</p>'
        },
        'multimediaEduardoBoza':{
            title:'Multimedia',
            html:'<p>Multimedia de EduardoBoza</p>'
        },
        'eventosEduardoBoza':{
            title:'Eventos',
            html:'<p>Eventos de EduardoBoza</p>'
        },
        'ubicacionEduardoBoza':{
            title:'Ubicación',
            html:'<p>Ubicación de EduardoBoza</p>'
        },
        'contactoEduardoBoza':{
            title:'Contacto',
            html:'<p>Contacto de EduardoBoza</p>'
        },
        'donativoEduardoBoza':{
            title:'Donativo',
            html:'<p>Donativo de EduardoBoza</p>'
        }
        /*CAMINO A LOS ALTARES - OPCIONES*/
    }
};