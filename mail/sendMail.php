<?php

header("Access-Control-Allow-Origin: *");

$ToEmail = 'lrodriguezp.18@gmail.com';
$Name  = 'CEV';
$UserEmail = $_POST['from'];
$SubjectEmail = 'Soporte Camino a los altares';
$BodyEmail = $_POST['body'];

$msg = array( 
  'success' => 'Exito', 
  'error'=>'Error',
  'emailSended'=>'Correo enviado exitosamente', 
  'emailNotSended'=>'Error enviando el correo' 
);

function SendMail( $ToEmail, $Name, $UserEmail, $SubjectEmail, $BodyEmail ) 
{
  require_once ( 'PHPMailer/PHPMailerAutoload.php' ); // Add the path as appropriate
  $Mail = new PHPMailer();
  $Mail->IsSMTP(); // Use SMTP
  $Mail->Host        = "smtp.gmail.com"; // Sets SMTP server
  $Mail->SMTPDebug   = 2; // 2 to enable SMTP debug information
  $Mail->SMTPAuth    = TRUE; // enable SMTP authentication
  $Mail->SMTPSecure  = "tls"; //Secure conection
  $Mail->Port        = 587; // set the SMTP port
  $Mail->Username    = 'lrodriguezp.18@gmail.com'; // SMTP account username
  $Mail->Password    = 'luarropa2894'; // SMTP account password
  $Mail->Priority    = 1; // Highest priority - Email priority (1 = High, 3 = Normal, 5 = low)
  $Mail->CharSet     = 'UTF-8';
  $Mail->Encoding    = '8bit';
  $Mail->Subject     = $SubjectEmail;
  $Mail->ContentType = 'text/html; charset=utf-8\r\n';
  $Mail->From        = $ToEmail;
  $Mail->FromName    = $Name;
  $Mail->WordWrap    = 900; // RFC 2822 Compliant for Max 998 characters per line

  $Mail->AddAddress( $ToEmail ); // To:
  $Mail->isHTML( TRUE );
  $Mail->Body    = 'El usuario ['.$UserEmail.'] ha enviado el siguiente correo: '.$BodyEmail;
  //$Mail->AltBody = $MessageTEXT;
  $Mail->Send();
  $Mail->SmtpClose();

  if ( $Mail->IsError() ) { // ADDED - This error checking was missing
    return FALSE;
  }
  else {
    return TRUE;
  }
}

$Send = SendMail( $ToEmail, $Name, $UserEmail, $SubjectEmail, $BodyEmail );

if ( $Send ) 
{
  $responseCode = 200;
  $status = 'success';
  $responseMsg = 'emailSended';
}
else 
{
  $responseCode = 400;
  $status = 'error';
  $responseMsg = 'emailNotSended';
}

$response = array('responseCode' => $responseCode, 'message' => $msg[$responseMsg], 'title' => $msg[$status]);
echo json_encode($response);

die;
?>